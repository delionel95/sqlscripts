USE ModernWays;
CREATE TABLE Taken (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Omschrijving varchar(50) NOT NULL
);
CREATE TABLE Leden (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Voornaam varchar(50) NOT NULL
);